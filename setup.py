# -*- coding: utf-8 -*-

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

readme = ""

setup(
    long_description=readme,
    name="hail",
    version="1.0.0",
    description="FlakeID library",
    python_requires="==3.*,>=3.7.0",
    author="elixi.re",
    author_email="python@elixi.re",
    license="LGPLv3",
    packages=["hail"],
    package_dir={"": "."},
    package_data={"hail": ["py.typed"]},
    install_requires=[],
)
