# hail

flakeid python library

## how

```sh
pip install https://gitlab.com/elixire/hail.git
```

```py
from hail import FlakeFactory

# first argument is the node id for that factory
factory = FlakeFactory(1234)

factory.get_flake()

# you can also generate flakes manually
import time
from hail import Flake

my_flake = Flake.from_vars(int(time.time() * 1000), 0, 0)
print(my_flake.timestamp, my_flake.node_id, my_flake.seq)
print(my_flake.as_uuid)
```
