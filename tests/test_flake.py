import random
from hail import Flake, FlakeFactory


def test_flake():
    genned_node_id = random.randint(10, 10000)
    factory = FlakeFactory(genned_node_id)
    flake = factory.get_flake()

    assert isinstance(str(flake), str)
    assert flake.node_id == genned_node_id
    assert flake.seq == 0

    assert flake.num == flake.from_uuid(flake.as_uuid).num
