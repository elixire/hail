# hail: FlakeID library
# Copyright 2020, elixi.re Team and the winter contributors
# SPDX-License-Identifier: LGPLv3

import time
from .flake import Flake


class FlakeFactory:
    def __init__(self, node_id: int):
        self.node_id = node_id
        self.seq = 0

    def get_flake(self) -> Flake:
        """Generate a Flake."""
        flake = Flake.from_vars(int(time.time() * 1000), self.node_id, self.seq)
        self.seq += 1
        return flake
