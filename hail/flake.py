# hail: FlakeID library
# Copyright 2020, elixi.re Team and the winter contributors
# SPDX-License-Identifier: LGPLv3

import uuid


class Flake:
    """Represents a single Flake ID."""

    __slots__ = "num"

    def __init__(self, num: int):
        self.num = num & ((1 << 128) - 1)

    @classmethod
    def from_vars(self, timestamp: int, node_id: int, seq: int) -> "Flake":
        # timestamp is 64 bits
        # node_id is 48 bits
        # seq is 16 bits

        # end result is 128 bits
        return Flake(
            ((timestamp & 0xFFFFFFFFFFFFFFFF) << 64)
            | ((node_id & 0xFFFFFFFFFFFF) << 16)
            | (seq & 0xFFFF)
        )

    @classmethod
    def from_string(cls, string: str) -> "Flake":
        return cls(uuid.UUID(string).int)

    @classmethod
    def from_uuid(cls, incoming: uuid.UUID) -> "Flake":
        return cls(incoming.int)

    @property
    def timestamp(self) -> int:
        return (self.num >> 64) & 0xFFFFFFFFFFFFFFFF

    @property
    def node_id(self) -> int:
        return (self.num >> 16) & 0xFFFFFFFFFFFF

    @property
    def seq(self) -> int:
        return self.num & 0xFFFF

    @property
    def as_uuid(self) -> uuid.UUID:
        return uuid.UUID(int=self.num)

    def __repr__(self) -> str:
        return f"Flake<{self.as_uuid.hex}>"

    def __str__(self) -> str:
        return self.as_uuid.hex

    def __int__(self) -> int:
        return self.num
